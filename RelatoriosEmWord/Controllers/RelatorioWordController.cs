﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Http;
using Microsoft.Office.Interop.Word;
using TemplateEngine.Docx;

namespace RelatoriosEmWord.Controllers
{
    public class RelatorioWordController : ApiController
    {
        [HttpGet]
        [Route("CertidaoSecretariaJudiciaria")]
        public string Teste()
        {
            var basePathTemplates = System.Web.HttpContext.Current.Server.MapPath("~/TemplatesWord");
            var basePathResults = System.Web.HttpContext.Current.Server.MapPath("~/TmpResults");
            var templateDoc = Path.Combine(basePathTemplates, "CertidaoSecJudiciaria.docx");
            var docResult = Path.Combine(basePathResults, "ResExemplo1.docx");
            var pdfResult = Path.Combine(basePathResults, "ResExemplo1.pdf");

            File.Delete(docResult);
            File.Copy(templateDoc, docResult);

            #region Prepara Lista de Conteudos

            var NumeroEdital = new FieldContent("NumeroEdital", "001");
            var TipoEdital = new FieldContent("TipoEdital", "Promoção");
            var CriterioEdital = new FieldContent("CriterioEdital", "Merecimento");
            var VagaEdital = new FieldContent("VagaEdital", "1ª Vara Cível e Empresarial de Belém");
            var ComarcaEdital = new FieldContent("ComarcaEdital", "Belém");
            var NumeroDiarioJustica = new FieldContent("NumeroDiarioJustica", "GP-947/2019");
            var DataPublicacaoDiario = new FieldContent("DataPublicacaoDiario", DateTime.Now.AddMonths(-1).Date.ToString("dd/MM/yyyy"));
            var DataFimInscricaoEdital = new FieldContent("DataFimInscricaoEdital", DateTime.Now.AddDays(-1).Date.ToString("dd/MM/yyyy"));

            var TabelaCandidatos = new TableContent("TabelaCandidatos")
                 .AddRow(
                        new FieldContent("Candidato", "FULANO DA SILVA"),
                        new FieldContent("Entrancia", "2ª"),
                        new FieldContent("Comarca", "Ananindeua"),
                        new FieldContent("Vara", "1ª Vara Cível e Empresarial de Ananindeua"),
                        new FieldContent("Nivel", "Juiz Titular")
                )
                .AddRow(
                        new FieldContent("Candidato", "XICO BOY"),
                        new FieldContent("Entrancia", "2ª"),
                        new FieldContent("Comarca", "Marituba"),
                        new FieldContent("Vara", "1ª Vara Cível e Empresarial de Marituba"),
                        new FieldContent("Nivel", "Juiz Titular")
                );

            var TextoProcessoAdministrativo = new FieldContent("TextoProcessoAdministrativo", "- Inexiste qualquer Procedimento Administrativo Disciplinar em nome dos demais Magistrados inscritos.");
            var TabelaProcessoAdministrativo = new TableContent("TabelaMagistradosProcessoAdministrativo").Hide();

            // TODO Existe Processo Administrativo ?
            //TextoProcessoAdministrativo.Value = "Existe(m) Processo(s) Administrativo(s) para o(s) Magistrado(s) abaixo listado(s):";
            //TabelaProcessoAdministrativo
            //        .AddRow(
            //            new FieldContent("MagistradoProcAdmin", "FULANO DA SILVA"),
            //            new FieldContent("JustificativaProcAdmin", "Conforme Processo CNJ Nº 1111"))
            //        .AddRow(
            //            new FieldContent("MagistradoProcAdmin", "XICO DA SILVA BOY"),
            //            new FieldContent("JustificativaProcAdmin", "Conforme Processo CNJ Nº 2222"));

            var TextoInfracaoPenal = new FieldContent("TextoInfracaoPenal", "- Inexiste qualquer Procedimento Criminal em nome dos demais Magistrados inscritos.");
            var TabelaInfracaoPenal = new TableContent("TabelaInfracaoPenal").Hide();

            // TODO Existe Infracao Penal ?
            //TextoInfracaoPenal.Value = "Existe(m) Processo(s) por Infração(ões) Penal(is) para o(s) Magistrado(s) abaixo listado(s):";
            //TabelaInfracaoPenal
            //        .AddRow(
            //            new FieldContent("MagistradoInfracaoPenal", "FULANO DA SILVA"),
            //            new FieldContent("JustificativaInfracaoPenal", "Conforme Processo CNJ Nº 1111"))
            //        .AddRow(
            //            new FieldContent("MagistradoInfracaoPenal", "XICO DA SILVA BOY"),
            //            new FieldContent("JustificativaInfracaoPenal", "Conforme Processo CNJ Nº 2222"));

            var TextoPenalidade = new FieldContent("TextoPenalidade", "- Inexiste qualquer Penalidade em nome dos demais Magistrados inscritos.");
            var TabelaPenalidade = new TableContent("TabelaPenalidade").Hide();

            // TODO Existe Penalidade ?
            //TextoPenalidade.Value = "Existe(m) Processo(s) por Infração(ões) Penal(is) para o(s) Magistrado(s) abaixo listado(s):";
            //TabelaPenalidade
            //        .AddRow(
            //            new FieldContent("MagistradoPenalidade", "FULANO DA SILVA"),
            //            new FieldContent("JustificativaPenalidade", "Conforme Processo CNJ Nº 1111"))
            //        .AddRow(
            //            new FieldContent("MagistradoPenalidade", "XICO DA SILVA BOY"),
            //            new FieldContent("JustificativaPenalidade", "Conforme Processo CNJ Nº 2222"));

            #endregion

            #region Prepara Lista de Conteudos e configura na Engine

            var conteudos = new List<IContentItem>();
            conteudos.Add(NumeroEdital);
            conteudos.Add(TipoEdital);
            conteudos.Add(CriterioEdital);
            conteudos.Add(VagaEdital);
            conteudos.Add(ComarcaEdital);
            conteudos.Add(NumeroDiarioJustica);
            conteudos.Add(DataPublicacaoDiario);
            conteudos.Add(DataFimInscricaoEdital);
            conteudos.Add(TabelaCandidatos);

            conteudos.Add(TextoProcessoAdministrativo);
            conteudos.Add(TextoInfracaoPenal);
            conteudos.Add(TextoPenalidade);
            conteudos.Add(TabelaProcessoAdministrativo);
            conteudos.Add(TabelaInfracaoPenal);
            conteudos.Add(TabelaPenalidade);

            var valuesToFill = new Content(conteudos.ToArray());

            #endregion

            #region Processa Template com o Conteudo

            using (var outputDocument = new TemplateProcessor(docResult).SetRemoveContentControls(true))
            {
                outputDocument.FillContent(valuesToFill);
                outputDocument.SaveChanges();
            }

            #endregion

            Docx2Pdf(docResult, pdfResult);

            return "OK";
        }

        private void Docx2Pdf(string docfile, string pdfDestino)
        {
            var appWord = new Application();
            var wordDocument = appWord.Documents.Open(docfile);
            wordDocument.ExportAsFixedFormat(pdfDestino, WdExportFormat.wdExportFormatPDF);
        }
    }
}
